#include <stdio.h>

#include "includes/perfect_square.h"

#define UVOD_HLASKA "********Test program*********\n"

int main(void)
{
    int num;

    printf(UVOD_HLASKA);
    printf("Input number: ");
    scanf("%d", &num);

    is_perfect_square(num) ? printf("number is perfect square\n") :
	printf("number is not perfect square\n");

    return 0;
}
