#include "includes/perfect_square.h"

#include <stdio.h>
#include <math.h>

int is_perfect_square(int num)
{
    int iVar;
    float fVar;

    fVar = sqrt((double)(num));
    iVar = fVar;

    return ((iVar == fVar) ? 1 : 0);
}
