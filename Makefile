CC=gcc
CFLAGS=-I.
LDFLAGS=-lm
TARGET=is_square
OBJ=main.o sources/perfect_square.o
DEPS=includes/perfect_square.h

all:$(TARGET)

#kompilacia o suborov z c suborov
%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

#linkovanie
$(TARGET): $(OBJ)
	$(CC) -o $@ $^ $(LDFLAGS)
	echo "vsetko hotovo"

#clean
clean:
	rm -rf $(TARGET)
	rm -rf *.o
	rm -rf sources/*.o













