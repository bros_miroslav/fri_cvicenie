#ifndef _PERFECT_SQUARE_H_
#define _PERFECT_SQUARE_H_

int is_perfect_square(int num);

#endif
